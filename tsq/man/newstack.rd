\name{newstack, push.stack, pop.stack, print.stack}
\alias{newstack}
\alias{push}
\alias{pop}
\alias{print}

\title{tsq Stack}

\description{
Stack data structure. Stacks use the Last-in-first-out principle when adding
and removing values.
}

\usage{
newstack()
push.stack(x, val)
pop.stack(x)
print.stack(stack)
}

\arguments{
	\item{x}{An instance of a stack object}
	\item{val}{The value that will be appended to the top of the stack}
	\item{stack}{An instance of a stack object}
}

\details{Creates a new stack object. Values can be pushed to the top of the
	stack using the \code{push} method. The \code{pop} method removes the
	value at the top of the stack and returns it. The \code{print} method 
	prints the contents of the stack, from the most recently item added to
        the least recently added item. 	
}


\value{The \code{newstack} method returns an object of type \code{"stack"}.
        The \code{pop} function returns the most recently added item, which is
	the top of the stack.
}


\examples{
S = newstack()
push.stack(S, 25)
push.stack(S, 10)
push.stack(S, 3)
pop.stack(S)
print.stack(S)
S2 = S
}

\author{
Beshoy Youssef, Motez Musa, Alex Leshchuk, Matt Quesada
}
