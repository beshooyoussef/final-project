\name{newbintree, push.bintree, pop.bintree, print.bintree}
\alias{newbintree}
\alias{push}
\alias{pop}
\alias{print}

\title{tsq Bintree}

\description{
Binary Tree data structure. Each node contains a data value. Left subtrees
consist of nodes whose values are less than the parent value, so Left < Parent.
Right subtrees consist of nodes whose values are greater than the parent value,
so Parent < Right.
}

\usage{
newbintree()
push.bintree(x, val)
pop.bintree(x)
print.bintree(bt)
}

\arguments{
	\item{x}{An instance of a binary tree.}
	\item{val}{The value that will be appended to the tree.}
	\item{bt}{An instance of a binary tree.}
}

\details{Creates a new bintree object. Values can be pushed using the \code{push}
	method. The \code{pop} method removes the node containing the smallest value
	from the tree. The \code{print} method prints the binary tree in sorted
	ascending order.
}


\value{ The \code{newbintree} function returns an object of type \code{"bintree"}.
	The \code{pop} function returns the smallest value in the tree.
}


\examples{
B = newbintree()
push.bintree(B, 25)
push.bintree(B, 10)
push.bintree(B, 3)
pop.bintree(B)
print.bintree(B)
B2 = B
}

\author{
Beshoy Youssef, Motez Musa, Alex Leshchuk, Matt Quesada
}
