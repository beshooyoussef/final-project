\name{newqueue, push.queue, pop.queue, print.queue}
\alias{newqueue}
\alias{push}
\alias{pop}
\alias{print}

\title{tsq Queue}

\description{
Queue data structure. Queues use the first-in-first-out principle when adding
and removing values. 
}

\usage{
newqueue()
push.queue(x, val)
pop.queue(x)
print.queue(q)
}

\arguments{
	\item{x}{An instance of a queue}
	\item{val}{The value that will be appended to the end of the queue}
	\item{q}{An instance of a queue}
}

\details{Creates a new queue object. Values can be pushed to the end of the
	queue using the \code{push} method. The \code{pop} method removes the
	first item in the queue and returns it. The \code{print} method prints 
	the contents of the queue, from the least recently added item to the most 
	recently added item.
}


\value{The \code{newqueue} method returns an object of type \code{"queue"}.
	The \code{pop} function returns the least recently added item.
}


\examples{
Q = newqueue()
push.queue(Q, 25)
push.queue(Q, 10)
push.queue(Q, 3)
pop.queue(Q)
print.queue(Q)
Q2 = Q
}

\author{
Beshoy Youssef, Motez Musa, Alex Leshchuk, Matt Quesada
}
