\documentclass{article}
\usepackage{graphicx}

\begin{document}

\title{Trees, Stacks, and Queues in R}
\author{Motez Musa \\Beshoy Youssef \\Alex Leshchuk \\Matthew Quesada}
\date{March 21, 2017}
\maketitle


\section{Introduction}
\qquad The following report details the design of our stack, queue, and binary tree implementation using R. In addition, we discuss the use of references classes (in comparison to R3 and others), challenges of implementation, comparisons with Python, and testing methods




\section{Reference Classes vs. Others}
\qquad Objects in R provide an easy way to implement abstraction and organization in programs. S3 provides more flexibility and ease of use, whereas S4 enables structured programs. Newer additions like reference classes and R6 support mutability.\\\\\null
\qquad S3 uses “generic-function OO.” This type of object-oriented programming differs from other OO programming languages (Java and C++) which use message passing. S4 adds formality to S3 by providing formal class definitions and allowing multiple dispatch. Typically, S3 and S4 are used for static objects that are not self-modifying. For this project, we refrained from using S3/S4 classes due to the inherent self-modifying nature of stacks, queues, and trees. Implementing these data structures in S3 or S4 would require extra overhead and numerous reassignments so that we can use the data returned from our functions to update our global structures.\\\\\null
\qquad Reference classes avoid these issues altogether. By implementing message-passing, methods now belong to classes. Because stacks, queues, and trees are constantly changing based on their input, we want to use an OO system that supports mutable types.  With reference classes, R creates a full copy of the object each time. This OO system is much closer to C++ and Java. R6 is similar to R’s reference classes, but adds more functionality such as public and private methods, active bindings, and inheritance. For this project, reference classes alone would suffice

\section{Challenges of Implementation}
\qquad Due to R’s lack of a pointer data type, data structures are harder to implement. Without pointers to abstract objects likes nodes or heads/tails, we are forced to implement these using lists and matrices. Design choices will be described later in the report. Thankfully, R’s reference class OO system closely resembles OO functionality of C++ and Java. Thus, we can avoid the generic-function OO of S3/S4 and the reassignment over-head that accompanies it. This also makes our R implementation closer to that of Python, which uses the self-variable to permit self-modification. With R, we use super-assignment and reference classes to achieve the same goal.



\section{Design}

\subsection{Stack}
\qquad Our stack is implemented as a reference class with methods acting on a vector called “stk.” Using a reference class allows us to change our stack within our methods. The top of the stack is represented as a numeric type and keeps track of how many elements are in the stack, so that we can readily index to that position to access it.\\\null
\qquad The pop function simply takes the value from the “top” index of the “stk” vector and decrements the top variable. It then returns that value. 
\begin{verbatim}
	temp = stk[top]  
	top <<- top - 1
	return(temp) 						#return top of stack
\end{verbatim}
\qquad The push function increments the top variable by 1, and then adds the argument supplied to push to our stack at the top index. 
\begin{verbatim}
top <<- top + 1 
	stk[top] <<- val						#set top index to value

\end{verbatim}
\qquad Finally, the print function iterates through our vector and prints the value at that index (starting from the top index). It then decrements a counter each time to update the new index for the next iteration. 
\begin{verbatim}
counter = top 
while(counter > 0){
    cat(stk[counter], "\t")				#print values	
    counter =  counter - 1
}
\end{verbatim}
\subsection{Queue}
\qquad Our queue is implemented as a reference class, with the queue represented as a vector called “qu”, and two “numeric” types called front and rear to simulate the front and rear indices of the queue. Using a reference class allows us to change our queue within our methods.
\qquad The pop function checks if the “front” and “rear” are both set to the same index, and if so, returns that the queue is empty. Otherwise, pop gets the value at the “front” index and reassigns the queue to qu[-1] to omit the first index. The rear value also decrements, and the pop value is returned. 
\begin{verbatim}
temp = qu[front]
qu <<- qu[-1]						#reassign q to omit position 1
rear <<- rear - 1						#change rear value
return(temp)

\end{verbatim}
\qquad Push takes an argument called val, and sets the index corresponding to “rear” to the given argument. The rear index is then incremented to simulate the size increase of the queue.
\begin{verbatim}
qu[rear] <<- val 			
rear <<- rear + 1					#increase size of queue by 1
\end{verbatim}
\qquad The print function uses a counter (set to “front”), and iterates through our queue until it reaches the “rear” index. At each iteration, it prints the value of the queue. 
\begin{verbatim}
counter = front						#start at the front of the queue
while(counter < rear)			
{
    cat(qu[counter], "\t")					#print queue contents at given index
    counter =  counter + 1
}
\end{verbatim}

\subsection{Binary Tree}
\qquad Our binary tree data structure uses a reference class like the other two data structures described above. This data structure is represented by a four-column matrix. The columns are named value, left child, right child, and parent respectively. Each row in our matrix represents a node in our tree, with the indices of its children and parent stored in their respective columns to allow traversals through our tree. The pop and push function use if-else statements to run through the edge cases that are possible in the trees. We push duplicate values as the left child of the same value already in the tree. \\\null
\qquad We start by traversing the left branches of the tree, since the smallest value to pop will always be to the left of the current node (assuming the current node isn’t the smallest).
\begin{verbatim}
while(!is.na(tree[i,2])) i = tree[i,2] 			#traverse left children
\end{verbatim}
\qquad Through each iteration, we check if there are no left and right children on the current node. Once we reach our node to pop, we simply reset child values in the parent node to NA (depending on whether our node was a left node or right node). 
\begin{verbatim}

if(is.na(tree[i,2]) & is.na(tree[i,3]))			#if no left or right child on current node
....
....  
if(tree[parent,2] == i) 			    #if current node is left child
{
    tree[parent,2] <<- NA		   	    #reset parent value
}
\end{verbatim}
The same check is done for the case of a right child. \\\null
\qquad Lastly, if our tree needs to be restructured, we reassign the parent node’s children values accordingly. 
In the case of a left child:
\begin{verbatim}
else if(tree[parent,2] == i) 
{
    if(!is.na(leftChild)) 
    {
        tree[parent,2] <<- leftChild 		#reset parents left child
    }
....
\end{verbatim}
\qquad The push function also loops through the tree, this time comparing the argument value to be pushed to the value of each node. Left children consist of values smaller than its parent, and right children consist of values greater than its parent. Therefore, we check if the value is less than or greater than the current node and iterate from there. For example, if our value is less than our current node, and there are no left children, we can simply add our node to the matrix. 
\begin{verbatim}
if (is.na(tree[i,2])) 				#base case: if no left child
{
    tree <<- rbind(tree, c(val, NA, NA,parent)) 	#add to matrix
    tree[i,2] <<- length(tree[,1]) #
}
\end{verbatim}
Otherwise, we continue to traverse until we find the appropriate node to append to, then do our assignment and update the parent.
\begin{verbatim}
curLeftChild = tree[i,2]
		cur = i
		tree <<- rbind(tree, c(val, curLeftChild, NA,i))
		tree[curLeftChild, 4] <<- length(tree[,1])
		tree[i, 2] <<- length(tree[,1])

\end{verbatim}
\qquad To print our tree, we call a recursive in-order print function to bring the tree contents in order, scanning left nodes before scanning right. 

\section{Testing}
See the man pages for testing examples.

\section{Sources}
The following sources were referenced to find the pros and cons of different OO systems in R. \\\\
https://rpubs.com/wch/24456 \\
http://adv-r.had.co.nz/OO-essentials.html\\
http://www2.hawaii.edu/~mbutler/PDFs/Ch9.S3vsS4.pdf\\

\end{document}
